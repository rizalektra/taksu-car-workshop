﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TaksuCarWorkshop.Areas.Identity.Data;
using TaksuCarWorkshop.Data;

[assembly: HostingStartup(typeof(TaksuCarWorkshop.Areas.Identity.IdentityHostingStartup))]
namespace TaksuCarWorkshop.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<TaksuCarContext>(options =>
                    options.UseMySql(
                        context.Configuration.GetConnectionString("CWSContextConnection"), ServerVersion.AutoDetect(context.Configuration.GetConnectionString("CWSContextConnection"))));

                services.AddDefaultIdentity<TaksuCarUser>(options => options.SignIn.RequireConfirmedAccount = true)
                    .AddEntityFrameworkStores<TaksuCarContext>();
            });
        }
    }
}