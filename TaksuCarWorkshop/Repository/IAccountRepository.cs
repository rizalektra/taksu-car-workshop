﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaksuCarWorkshop.Models;

namespace TaksuCarWorkshop.Repository
{
    public interface IAccountRepository
    {
        Task<ApplicationUserModel> GetUserByEmailAsync(string email);

        Task<SignInResult> PasswordSignInAsync(SignInModel signInModel);

        Task SignOutAsync();

        Task<IdentityResult> ConfirmEmailAsync(string uid, string token);
    }
}
