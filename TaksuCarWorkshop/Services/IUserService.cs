﻿namespace TaksuCarWorkshop.Services
{
    public interface IUserService
    {
        string GetUserId();
        bool IsAuthenticated();
    }
}