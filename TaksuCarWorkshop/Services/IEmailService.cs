﻿using System.Threading.Tasks;
using TaksuCarWorkshop.Models;

namespace TaksuCarWorkshop.Services
{
    public interface IEmailService
    {
        Task sendRegistrationEmail(UserEmailOptionsModel userEmailOptions);
    }
}